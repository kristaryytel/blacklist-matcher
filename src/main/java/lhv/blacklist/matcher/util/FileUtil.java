package lhv.blacklist.matcher.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public final class FileUtil {

    private FileUtil() {
    }

    /**
     * Returns the lines of file as list of strings.
     *
     * @param location location of the file to be read in resources folder
     * @return file content as list of strings
     */
    public static List<String> getFileContent(String location) {
        try (InputStream inputStream = FileUtil.class.getClassLoader().getResourceAsStream(location)) {
            if (inputStream == null) {
                throw new RuntimeException(String.format("Failed to read file from location %s", location));
            }
            return IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to read file content from %s", location), e);
        }
    }
}
