package lhv.blacklist.matcher.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

public final class NameProcessingUtil {
    private static final String IGNORED_CHARACTERS_REGEX = "[,;()/]";

    private NameProcessingUtil() {
    }

    /**
     * Returns input name as parts where each part is in lowercase and has ignored characters, e.g. commas, and
     * extra whitespace removed.
     *
     * @param name the name to be cleaned
     * @return list of cleaned nonnull name parts
     */
    public static List<String> getCleanedNameParts(String name) {
        if (StringUtils.isBlank(name)) {
            return Collections.emptyList();
        }

        List<String> result = new ArrayList<>();
        for (String namePart : name.split(" ")) {
            result.add(StringUtils.trimToNull(
                    namePart.toLowerCase(Locale.ENGLISH).replaceAll(IGNORED_CHARACTERS_REGEX, "")));
        }
        return result.stream().filter(Objects::nonNull).distinct().collect(Collectors.toList());
    }

    /**
     * Removes noise words, e.g. pronouns, that are listed in given noise words file.
     *
     * @param inputWords list of words to be processed
     */
    public static void removeNoiseWords(List<String> inputWords, String noiseWordsFilename) {
        if (inputWords == null) {
            throw new IllegalArgumentException("Input list cannot be null");
        }

        List<String> noiseWords = FileUtil.getFileContent(noiseWordsFilename);
        inputWords.removeAll(noiseWords);
    }
}
