package lhv.blacklist.matcher;

import java.util.List;

public interface BlacklistNameMatchingService {

    List<String> getBlacklistMatches(String name, String blacklistFilename, String noiseWordsFilename);
}
