package lhv.blacklist.matcher;

import lhv.blacklist.matcher.util.FileUtil;
import lhv.blacklist.matcher.util.NameProcessingUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class BlacklistNameMatchingServiceImpl implements BlacklistNameMatchingService {
    private static final BigDecimal MATCHED_ACCURACY_MEAN_MIN = BigDecimal.valueOf(0.8);
    private static final BigDecimal UNMATCHED_SHARE_MAX = BigDecimal.valueOf(0.34);

    private static final double LEVENSHTEIN_DISTANCE_1_ACCURACY = 0.9;
    private static final double LEVENSHTEIN_DISTANCE_2_ACCURACY = 0.7;
    private static final double ABBREVIATION_ACCURACY = 0.6;
    private static final int LEVENSHTEIN_DISTANCE_2_WORD_LENGTH_MIN = 6;

    /**
     * Algorithm for finding matches for a name from a list of names.
     * <p>
     * First, input name is processed: ignored characters and extra whitespace is removed and name is split into parts.
     * Second, each blacklist name is compared against these input name parts. For this the blacklist name is also
     * split into components and for each component a match is searched from input name parts. Matching starts from the
     * most accurate method (finding the exact match) and continues with gradually less accurate methods (finding
     * a match by levenshtein distance and abbreviation).
     * <p>
     * A match is found if the share of unmatched blacklist name components don't exceed a threshold of 0.34 and
     * for matched components the mean match accuracy is at least 0.8 (on the scale of 0 to 1.0). As a result
     * the matching blacklist name is printed and added to the list of names to be returned.
     *
     * @param name               name to be compared against blacklist, , must not be null
     * @param blacklistFilename  name of file containing blacklist names, must not be null
     * @param noiseWordsFilename name of file containing noise words, must not be null
     * @return list of matching blacklist names
     */
    @Override
    public List<String> getBlacklistMatches(String name, String blacklistFilename, String noiseWordsFilename) {
        if (!StringUtils.isNoneBlank(name, blacklistFilename, noiseWordsFilename)) {
            throw new IllegalArgumentException("Name, blacklist filename and noise words filename must all be given!");
        }

        List<String> cleanedInputNameParts = NameProcessingUtil.getCleanedNameParts(name);
        NameProcessingUtil.removeNoiseWords(cleanedInputNameParts, noiseWordsFilename);

        List<String> blacklistNames = FileUtil.getFileContent(blacklistFilename);
        return blacklistNames.stream()
                .filter(blacklistName -> isMatch(blacklistName, new ArrayList<>(cleanedInputNameParts)))
                .peek(blacklistName -> System.out.println(String.format("Found match: %s", blacklistName)))
                .collect(Collectors.toList());
    }

    private boolean isMatch(String blacklistName, List<String> inputNameParts) {
        List<String> blacklistNameParts = Arrays.asList(blacklistName.toLowerCase(Locale.ENGLISH).split(" "));

        Map<String, BigDecimal> blacklistNamePartToMatchAccuracy =
                matchBlacklistNamePartsToInputNameParts(blacklistNameParts, inputNameParts);

        return getUnmatchedShare(blacklistNamePartToMatchAccuracy.values()).compareTo(UNMATCHED_SHARE_MAX) <= 0 &&
                getMatchedAccuracyMean(blacklistNamePartToMatchAccuracy.values())
                        .compareTo(MATCHED_ACCURACY_MEAN_MIN) >= 0;
    }

    private BigDecimal getUnmatchedShare(Collection<BigDecimal> matchAccuracies) {
        long unmatchedCount = matchAccuracies.stream()
                .filter(ma -> ma.compareTo(BigDecimal.ZERO) == 0)
                .count();
        return new BigDecimal(unmatchedCount).divide(new BigDecimal(matchAccuracies.size()), 10, RoundingMode.HALF_UP);
    }

    private BigDecimal getMatchedAccuracyMean(Collection<BigDecimal> matchAccuracies) {
        List<BigDecimal> matchedAccuracies = matchAccuracies.stream()
                .filter(ma -> ma.compareTo(BigDecimal.ZERO) > 0)
                .collect(Collectors.toList());

        return matchedAccuracies.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(matchedAccuracies.size()), 10, RoundingMode.HALF_UP);
    }

    private Map<String, BigDecimal> matchBlacklistNamePartsToInputNameParts(List<String> blacklistNameParts,
                                                                            List<String> inputNameParts) {
        Map<String, BigDecimal> result = blacklistNameParts.stream()
                .collect(Collectors.toMap(blacklistLastName -> blacklistLastName,
                        blacklistLastName -> BigDecimal.ZERO,
                        (duplicateName1, duplicateName2) -> duplicateName1));

        addExactMatches(result, inputNameParts);
        addNonExactMatches(result, inputNameParts);
        return result;
    }

    private void addExactMatches(Map<String, BigDecimal> blacklistNamePartToMatchAccuracy,
                                 List<String> inputNameParts) {
        blacklistNamePartToMatchAccuracy.keySet().forEach(blacklistNamePart -> {
            if (inputNameParts.contains(blacklistNamePart)) {
                blacklistNamePartToMatchAccuracy.replace(blacklistNamePart, BigDecimal.ONE);
                inputNameParts.remove(blacklistNamePart);
            }
        });
    }

    private void addNonExactMatches(Map<String, BigDecimal> blacklistNamePartToMatchAccuracy,
                                    List<String> inputNameParts) {
        blacklistNamePartToMatchAccuracy.entrySet().stream()
                .filter(entry -> entry.getValue().compareTo(BigDecimal.ZERO) == 0)
                .forEach(entry -> {
                    entry.setValue(getMatchByLevenshteinDistance(entry.getKey(), inputNameParts, 1,
                            LEVENSHTEIN_DISTANCE_1_ACCURACY));
                });

        blacklistNamePartToMatchAccuracy.entrySet().stream()
                .filter(entry -> entry.getValue().compareTo(BigDecimal.ZERO) == 0 &&
                        entry.getKey().length() >= LEVENSHTEIN_DISTANCE_2_WORD_LENGTH_MIN)
                .forEach(entry -> {
                    entry.setValue(getMatchByLevenshteinDistance(entry.getKey(), inputNameParts, 2,
                            LEVENSHTEIN_DISTANCE_2_ACCURACY));
                });

        blacklistNamePartToMatchAccuracy.entrySet().stream()
                .filter(entry -> entry.getValue().compareTo(BigDecimal.ZERO) == 0)
                .forEach(entry -> {
                    entry.setValue(getMatchByAbbreviation(entry.getKey(), inputNameParts));
                });
    }

    private BigDecimal getMatchByLevenshteinDistance(String blacklistNamePart,
                                                     List<String> inputNameParts,
                                                     int levenshteinDistanceMin,
                                                     double matchAccuracy) {
        Optional<String> match = inputNameParts.stream()
                .filter(inputNamePart -> {
                    Integer levenshteinDistance = new LevenshteinDistance().apply(inputNamePart, blacklistNamePart);
                    return levenshteinDistance <= levenshteinDistanceMin;
                }).findFirst();

        match.ifPresent(inputNameParts::remove);
        return match.isPresent() ? BigDecimal.valueOf(matchAccuracy) : BigDecimal.ZERO;
    }

    private BigDecimal getMatchByAbbreviation(String blacklistNamePart, List<String> inputNameParts) {
        Optional<String> match = inputNameParts.stream()
                .filter(inputNamePart -> {
                    String regex = String.format("^%s{1}\\.", blacklistNamePart.charAt(0));
                    return inputNamePart.matches(regex);
                }).findFirst();

        match.ifPresent(inputNameParts::remove);
        return match.isPresent() ? BigDecimal.valueOf(ABBREVIATION_ACCURACY) : BigDecimal.ZERO;
    }
}
