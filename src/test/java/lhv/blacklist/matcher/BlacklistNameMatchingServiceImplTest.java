package lhv.blacklist.matcher;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@ExtendWith(MockitoExtension.class)
class BlacklistNameMatchingServiceImplTest {

    @InjectMocks
    private BlacklistNameMatchingServiceImpl blacklistNameMatchingService;

    @Test
    void getBlacklistMatches_ThrowsIllegalArgumentException_IfNameIsBlank() {
        // given
        String name = "";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        Throwable result = catchThrowable(() ->
                blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename));

        // then
        assertThat(result).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getBlacklistMatches_ThrowsIllegalArgumentException_IfBlacklistFilenameIsBlank() {
        // given
        String name = "test";
        String blacklistFilename = " \n     ";
        String noiseWordsFilename = "noise_words.txt";

        // when
        Throwable result = catchThrowable(() ->
                blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename));

        // then
        assertThat(result).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getBlacklistMatches_ThrowsIllegalArgumentException_IfNoiseWordsFilenameIsBlank() {
        // given
        String name = "test";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = " \n\t     ";

        // when
        Throwable result = catchThrowable(() ->
                blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename));

        // then
        assertThat(result).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getBlacklistMatches_ReturnsEmptyList_IfBlacklistEmpty() {
        // given
        String name = "test";
        String blacklistFilename = "blacklist_names_empty.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfTwoExactPartsOfNameGiven() {
        // given
        String name = "Osama Laden";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfExactNameGiven() {
        // given
        String name = "Osama Bin Laden";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameInOtherOrderWithCommaGiven() {
        // given
        String name = "Bin Laden, Osama";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameInWrongOrderGiven() {
        // given
        String name = "Laden Osama Bin";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameWithNoiseWordsGiven() {
        // given
        String name = "to the osama bin laden";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameWithNoiseWordsGiven2() {
        // given
        String name = "mr. osama and mrs. bin laden";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameWithDuplicatePartsGiven() {
        // given
        String name = "osama laden bin bin laden osama";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNamesWith1LetterChangedGiven() {
        // given
        String name = "isama lasen bin";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNamesWith2LettersChangedGiven() {
        // given
        String name = "ARABSOAR Mannsor";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("ARBABSIAR Manssor");
    }

    @Test
    void getBlacklistMatches_ReturnsEmptyList_IfNameWithTooManyLettersChangedGiven() {
        // given
        String name = "abama lasens bn";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameWithAbbreviationGiven() {
        // given
        String name = "O. bin Laden";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("Osama Bin Laden");
    }

    @Test
    void getBlacklistMatches_ReturnsEmptyList_IfNameWithTooManyAbbreviationsGiven() {
        // given
        String name = "O. bin L.";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameWithSomePartsMissingGiven() {
        // given
        String name = "al YACUB I. Mohamed";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsExactly("AL YACOUB Ibrahim Salih Mohammed");
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMatch_IfNameWithTooManyPartsMissingGiven() {
        // given
        String name = "al YACUB Mohamed";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getBlacklistMatches_ReturnsListWithMultipleMatch_IfNameWithMultipleMatchesGiven() {
        // given
        String name = "shakur A. golam";
        String blacklistFilename = "blacklist_names.txt";
        String noiseWordsFilename = "noise_words.txt";

        // when
        List<String> result = blacklistNameMatchingService.getBlacklistMatches(name, blacklistFilename, noiseWordsFilename);

        // then
        assertThat(result).containsOnly("SHAKURI Ali Gholam", "CHAKURE Holam Hassan");
    }
}
