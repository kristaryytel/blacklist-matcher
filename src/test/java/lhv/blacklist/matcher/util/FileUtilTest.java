package lhv.blacklist.matcher.util;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class FileUtilTest {

    @Test
    void getFileContent_ThrowsRuntimeException_IfFileDoesNotExist() {
        // given
        String location = "not_existing.txt";

        // when
        Throwable result = catchThrowable(() -> FileUtil.getFileContent(location));

        // then
        assertThat(result).isInstanceOf(RuntimeException.class);
    }

    @Test
    void getFileContent_ReturnsEmptyList_IfFileIsEmpty() {
        // given
        String location = "empty_file.txt";

        // when
        List<String> result = FileUtil.getFileContent(location);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getFileContent_ReturnsFilledList_IfFileIsNotEmpty() {
        // given
        String location = "filled_file.txt";

        // when
        List<String> result = FileUtil.getFileContent(location);

        // then
        assertThat(result).containsOnly(
                "firstName1 lastName1",
                "firstName2 lastName2",
                "name3");
    }
}
