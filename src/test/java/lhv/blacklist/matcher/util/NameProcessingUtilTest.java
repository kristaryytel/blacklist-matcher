package lhv.blacklist.matcher.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class NameProcessingUtilTest {

    @Test
    void getCleanedNameParts_ReturnsEmptyList_IfInputNameIsNull() {
        // given
        String inputName = null;

        // when
        List<String> result = NameProcessingUtil.getCleanedNameParts(inputName);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getCleanedNameParts_ReturnsEmptyList_IfInputNameIsBlank() {
        // given
        String inputName = "  \t ";

        // when
        List<String> result = NameProcessingUtil.getCleanedNameParts(inputName);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void getCleanedNameParts_ReturnsListWithTwoItemsInLowercase_IfInputNameHasTwoParts() {
        // given
        String inputName = "Osama Laden";

        // when
        List<String> result = NameProcessingUtil.getCleanedNameParts(inputName);

        // then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).containsExactly("osama", "laden");
    }

    @Test
    void getCleanedNameParts_RemovesWhitespaceAroundNameParts_IfInputNameHasExtraWhitespace() {
        // given
        String inputName = "  Osama    \n  Laden   ";

        // when
        List<String> result = NameProcessingUtil.getCleanedNameParts(inputName);

        // then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).containsExactly("osama", "laden");
    }

    @Test
    void getCleanedNameParts_RemovesIgnoredCharacters_IfInputNameHasIgnoredCharacters() {
        // given
        String inputName = "Bin; (Laden), Osama";

        // when
        List<String> result = NameProcessingUtil.getCleanedNameParts(inputName);

        // then
        assertThat(result).containsExactly("bin", "laden", "osama");
    }

    @Test
    void getFileContent_ThrowsIllegalArgumentException_IfInputWordsListIsNull() {
        // given
        List<String> inputWords = null;
        String noiseWordsFilename = "noise_words.txt";

        // when
        Throwable result = catchThrowable(() -> NameProcessingUtil.removeNoiseWords(inputWords, noiseWordsFilename));

        // then
        assertThat(result).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void getFileContent_RemovesNothing_IfInputWordsListIsEmpty() {
        // given
        List<String> inputWords = Collections.emptyList();
        String noiseWordsFilename = "noise_words.txt";

        // when
        NameProcessingUtil.removeNoiseWords(inputWords, noiseWordsFilename);

        // then
        assertThat(inputWords).isEmpty();
    }

    @Test
    void getFileContent_RemovesNoiseWords_IfInputWordsListContainsNoiseWords() {
        // given
        List<String> inputWords = new ArrayList<>(Arrays.asList(
                "to", "the", "osama", "and", "an", "bin", "laden", "for", "you"));
        String noiseWordsFilename = "noise_words.txt";

        // when
        NameProcessingUtil.removeNoiseWords(inputWords, noiseWordsFilename);

        // then
        assertThat(inputWords).containsExactly("osama", "bin", "laden");
    }
}
